var cors = require('cors');
const jsonServer = require("json-server")
const auth = require("json-server-auth")


const app = jsonServer.create()
const router = jsonServer.router("db.json")
const port = process.env.PORT || 3001


app.db = router.db

const rules = auth.rewriter({
    "/users*": "/664/users$1",
    "/feedbacks*": "/660/feedbacks$1",
    "/schedules*": "/660/schedules$1"
})

app.use(cors())
app.use(rules);
app.use(auth);
app.use(router)
app.listen(port)


console.log("server is running on port", port)